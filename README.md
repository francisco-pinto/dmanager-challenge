Introduction
============

This document will guide you through the tasks we expect you to complete.

Summary
=======

The goal of this challenge is to develop a simple data pipeline that makes data available through a micro API.

The system can be summarised as follows:

1. A web scrapper running in the background dumps raw HTML data from a website into the local hard drive;
2. The raw HTML data is parsed into a clean format, preferably into non-nested JSON or SQL formats;
3. The parsed data is imported into a local relational database;
4. A simple Python microservice receives HTTP requests, queries the local database, and returns results in JSON format.

The entire pipeline, including the API, can run on localhost. There is no need to set up a remote server. Steps 1 and 2 can be combined into one, if you find it suitable.

Here's a simple diagram of the pipeline:

- Web scraper ⟶ Text processor ⟶ **Database storage** ⟵ RESTful API ⟵ Search queries

Specifications
==============

The pipeline must be entirely programmed in Python, except for some web scraping operations where JavaScript can be used.

The website we want to scrape is [MIT OpenCourseware](https://ocw.mit.edu/). You should limit scraping to no more than 10 course pages, and only the "Course Home" section of each page.

Take [Organic & Biomaterials Chemistry](https://ocw.mit.edu/courses/materials-science-and-engineering/3-034-organic-biomaterials-chemistry-fall-2005/index.htm) as an example. Your web scrapper should extract information such as:

- Course title
- List of instructors separated by title, first name and last name;
- Course number;
- Academic term;
- Academic year;
- Level (Undergraduate, Master, etc);
- Course description;
- Related list of topics.

Scraped data should be parsed, cleaned up, and imported into a local relational database (following known best practices). You may organise the data into as many tables as you deem suitable.

Parsed data should then be made available through a Python API that receives JSON-formated search queries through HTTP requests, and outputs JSON-formated results.

An example search query for *Graduate-level courses* in the area of *Architecture* would be: {'Topic':'Architecture', 'Level':'Graduate'}.

**Bonus:** You'll receive extra points if you allow courses to be searchable by keyword(s).

Technologies
============

To develop your data pipeline, you may use any Python libraries of your choice. Here are a few recommendations:

- **Web scraping:** Selenium, BeautifulSoup, phantomjs (JavaScript);
- **Text processing:** Regular expressions, rake-nltk;
- **Databases:** SQLite, MySQL;
- **Microservice:** Flask, Tornado.

Assessment
==========

We are looking for someone not only with experience but also self-motivated and able to develop creative solutions. We welcome thinking out of the box! This assessment is designed to challenge you, and provide an instructive and fun experience.

Extra kudos if you document your solution, either with comments on your code or in a separate documents' structure. For example, you can use sphinx to document relevant development decisions, commands, research, web links etc.

We like people who are able to learn and research. Even if you might not get the tasks completed, your notes will help us in the evaluation of your work.

Other notes
==========

- Please try and complete the assessment **within 1 week** of receiving it;
- Explain your solution in 5-10 lines;
- Create repository to track and share your work. Please link commits relevant to each task completed. We recommend you create a Bitbucket account and a private git repository (there's no cost to you). Invite "francisco-pinto" to your repository team;
- Share your results even if you don't finish;
- We understand there's more than one way to do things. If you're stuck, pick one and justify your choice.


Assumptions
===========

You are proficient in Python, and you understand the concepts of web scraping, text processing, and RESTful API.


Rules
=====

Please keep this assessment to yourself, i.e. don't share it with you colleagues, friends and the rest of the world.
Keep the repository private.

You, and you alone, are expected to be the unique author of the solutions for this assessment. You will be questioned on details of the solution you provide.


Feedback
========

Please feel free to provide any feedback you have about this the assessment or even propose any technical and not technical improvement.
